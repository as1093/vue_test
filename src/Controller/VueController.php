<?php

namespace Drupal\vue_test\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * An example controller.
 */
class VueController extends ControllerBase {

  /**
   * Returns a render-able array for a test page.
   */
  public function content() {

    // Do something with your variables here.
    $myText = 'This is not just a default text!';
    $myNumber = 1;
    $myArray = [1, 2, 3];

    return [
      // Your theme hook name.
      '#theme' => 'vue_test_theme_hook',
      // Your variables.
      '#variable1' => $myText,
      '#variable2' => $myNumber,
      '#variable3' => $myArray,
    ];
  }
}
