Add '.vue-wrapper' class to a tag in a theme template (ex: themes/bartik/templates/page.html.twig) or mount your Vue app to body tag.

### Helpful tips:
- Use ddev - https://ddev.readthedocs.io/en/stable/users/cli-usage/#drupal-9-composer-setup-example
  - clear cache: `ddev . drush cr`
- Uncheck aggregation in /admin/config/development/performance.
- Disable cache in sites/default/services.yml
   `cache: false`
