import { createApp } from 'vue'
import App from './App.vue'

let wrapper = window.document.querySelector('.vue-wrapper')

if (wrapper) {
  let app = window.document.createElement('div')
  app.setAttribute('id', 'app')
  wrapper.insertBefore(app, wrapper.childNodes[0])

  createApp(App).mount('#app')
}
